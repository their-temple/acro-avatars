# Acro Avatars

Plugin for Model-View-Controller for Avatars in Godot, plus a lot more! 

Originally called M(D(ETL)P)VAvatars...sooo many (acro)nyms...now Acro Avatars ;-)

## Intentions

Every Deity, People, Place or Thing in your Godot game is an Acro Avatar...

Every Avatar can become useful to any soul playing your game!

- Model-(Data-(Extract-Transform-Load)-Processor)-View-Controller
- Groupings - managers for multiple instances of the same/similar types
- State machine
  - States modify available Model methods and signals
- Notifications/signals
- Scriptable object/action/interactions
- BDD/TDD creatable via scripting as well
- Networking aware for client/server modeling
- Plug-and-play Acro Avatars from local, res:, user:/, remote web sites and pck files
- Integrate with [Logos](https://gitlab/their-temple/logos)

## Features

Only Intentions at the moment...

## Demo

See [Acro Avatars Demo](https://gitlab.com/their-temple/acro-avatars-demo)

## Installation

Eventually witll be a [Plugin](https://docs.godotengine.org/en/stable/tutorials/plugins/editor/installing_plugins.html) for [Godot](https://godotengine.org/)

## License

Copyright © 2021 [Their Temple](http://theirtemple.com/) and contributors

License 2021 same as [Godot](https://godotengine.org/) except for exceptions!
